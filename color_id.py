import Mode
import Image
import Bake
import Mesh
import bpy
import sys

def argParse(s):
    try:
        return int(s)
    except:
        return s

argv = sys.argv[sys.argv.index("--") + 1:]
options = {}

for arg in argv:
    arg = arg.split("=")
    prop = arg[0]
    value = argParse(arg[1])
    options[prop] = value

# Select elements
mesh_object = Mesh.MeshObject(options["object"])

# Create image
color_id_map = Image.create("color_id_map", options['width'], options['height'])
color_id_map.set_path(options["filepath"])

# Bake
bake = Bake.create_color_id(mesh_object, color_id_map)
bake.render()

# Render map
color_id_map.save()