import Mode
import Image
import Bake
import Mesh
import bpy
import sys

def argParse(s):
    try:
        return int(s)
    except:
        return s

argv = sys.argv[sys.argv.index("--") + 1:]
options = {}

for arg in argv:
    arg = arg.split("=")
    prop = arg[0]
    value = argParse(arg[1])
    options[prop] = value

print(options)

# Select elements
low_poly = Mesh.MeshObject(options["low"])
high_poly = Mesh.MeshObject(options["high"])
cage_poly = Mesh.MeshObject(options["cage"])

nmap = Image.create("nmap", options['width'], options['height'])
nmap.set_path(options["filepath"])

# Initialize bake
if (options["useGroups"]):
    # print("@@Baking using multigroups@@")
    bake = Bake.create_multigroups(high_poly, low_poly, cage_poly, nmap)
else:
    # print("@@Baking using single object@@")
    bake = Bake.create_simple(high_poly, low_poly, cage_poly, nmap)

# Render map
bake.render()
nmap.save()
