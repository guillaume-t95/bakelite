const bakelite = require('./bakelite')

// bakelite.normal({
//   sourcefile: 'test/simple-bake.blend',
//   high: 'cube_high',
//   low: 'cube_low',
//   cage: 'cube_cage',
//   width: 2048,
//   height: 2048,
//   filepath: 'test/cube.png'
// })

// Normal bake
bakelite.normal({
  sourcefile: 'test/simple-bake.blend',
  high: 'flat_high',
  low: 'flat_low',
  cage: 'flat_cage', // Useless in this case
  width: 2048,
  height: 2048,
  filepath: 'test/flat.png'
})

// Normal baking with vertex group
bakelite.normal({
  sourcefile: 'test/vertex-groups.blend',
  high: 'high',
  low: 'low',
  cage: 'cage',
  width: 2048,
  height: 2048,
  filepath: 'test/vertex-group.png',
  useGroups: true
})


// Standard color ID map bake
bakelite.colorID({
  sourcefile: 'test/color_id.blend',
  object: 'cube',
  width: 2048,
  height: 2048,
  filepath: 'test/color-id-standard.png',
  useGroups: false
})

// Color ID map bake using vertex groups
// bakelite.colorID({
//   sourcefile: 'test/vertex-groups.blend',
//   object: "some_object",
//   width: 2048,
//   height: 2048,
//   filepath: 'test/color-id-vertex-groups.png',
//   useGroups: true
// })
