import bpy

def use_cycles():
    bpy.context.scene.render.engine = 'CYCLES'

def use_blender_render():
    bpy.context.scene.render.engine = 'BLENDER_RENDER'