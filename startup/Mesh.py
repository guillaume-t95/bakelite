import bpy
import Mode

def get_object(name):
    return MeshObject(name)

def select_all():
    bpy.ops.object.select_all('INVOKE_DEFAULT', action='SELECT')

def deselect_all():
    bpy.ops.object.select_all('INVOKE_DEFAULT', action='DESELECT')

def hide_all():
    for ob in bpy.context.scene.objects:
        ob.hide = True

def show_all():
    for ob in bpy.context.scene.objects:
        ob.hide = False

class VertexGroup:
    def __init__(self, edit, name):
        self.edit = edit
        self.mesh_object = self.edit.mesh_object
        self.vertex_groups = self.mesh_object.obj.vertex_groups
        self.index = self.vertex_groups[name].index
        self.name = name

    def set_active(self):
        self.vertex_groups.active_index = self.index

    def select(self):
        self.set_active()
        bpy.ops.object.vertex_group_select()

    def deselect(self):
        self.set_active()
        bpy.ops.object.vertex_group_deselect()

    def to_object(self, name):
        self.select()
        bpy.ops.mesh.separate(type='SELECTED')
        self.edit.deselect_all()
        Mode.to_object()
        self.mesh_object.deselect()
        bpy.context.selected_editable_objects[0].name = name
        deselect_all
        return MeshObject(bpy.context.selected_editable_objects[0].name)


class MeshEdit:
    def __init__(self, mesh_object):
        self.mesh_object = mesh_object
        self.deselect_all()

    def select_all(self):
        bpy.ops.mesh.select_all(action='SELECT')

    def deselect_all(self):
        bpy.ops.mesh.select_all(action='DESELECT')

    def vertex_groups(self):
        groups = []
        for group in self.mesh_object.obj.vertex_groups:
            groups.append(self.vertex_group(group.name))
        return groups

    def vertex_group(self, name):
        return VertexGroup(self, name)

class MeshObject:
    def __init__(self, name):
        if (bpy.data.objects[name]):
            self.obj = bpy.data.objects[name]
            self.name = name

    def edit(self):
        Mode.to_object()
        deselect_all()
        self.select()
        self.set_active()
        self.show()
        Mode.to_edit()
        return MeshEdit(self)

    def set_active(self):
        bpy.context.scene.objects.active = self.obj

    def hide(self):
        self.obj.hide = True

    def show(self):
        self.obj.hide = False

    def select(self):
        self.obj.select = True

    def deselect(self):
        self.obj.select = False

    def toggle_select(self):
        self.obj.select = not self.obj.select

    def vertex_groups(self):
        edit = self.edit()
        return edit.vertex_groups()

    def vertex_group(self, name):
        edit = self.edit()
        return edit.vertex_group(name)

    def material_count(self):
        return len(self.obj.material_slots)

    def assign_material_slot(self, slot, material):
        self.obj.material_slots[slot].material = material.material

    def assign_material(self, material):
        if self.material_count() < 1:
            self.obj.data.materials.append(material.material)
        else:
            self.obj.material_slots[0].material = material.material

    def translate(self, x, y, z):
        self.set_active()
        bpy.ops.transform.translate(value=(x, y, z), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)

