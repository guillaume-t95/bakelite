import bpy
import Color

material_uid = 0
shadeless_id = 0

shadeless_colors = [
    Color.create(1.0, 0.0, 0.0, 1.0),
    Color.create(0.0, 1.0, 0.0, 1.0),
    Color.create(0.0, 0.0, 1.0, 1.0),
    Color.create(1.0, 0.0, 1.0, 1.0),
    Color.create(1.0, 1.0, 0.0, 1.0),
    Color.create(0.0, 1.0, 1.0, 1.0)
]

def create_normal(name):
    return NormalBakeMaterial(name)

def create_shadeless():
    return ShadelessMaterial()

class ShadelessMaterial:
    def __init__(self):
        global material_uid, shadeless_id, shadeless_colors

        # Create material
        material_name = "shadeless_" + str(material_uid)
        material_uid += 1

        material = bpy.data.materials.new(material_name)
        material.use_nodes = True

        # The material output node is directly accessible by its name
        output_node = material.node_tree.nodes["Material Output"]

        # Create emission node and link it to the shader output
        emission_node = material.node_tree.nodes.new("ShaderNodeEmission")
        material.node_tree.nodes.active = emission_node
        material.node_tree.links.new(emission_node.outputs["Emission"], output_node.inputs["Surface"])

        # Assign color
        color_value = emission_node.inputs["Color"].default_value
        color = shadeless_colors[shadeless_id]
        color.assign_value(color_value)
        shadeless_id += 1

        # Create image node
        image_node = material.node_tree.nodes.new("ShaderNodeTexImage")

        self.material = material
        self.image_node = image_node

    def set_image(self, image):
        self.image_node.select = True
        self.material.node_tree.nodes.active = self.image_node
        self.image_node.image = image.image


class NormalBakeMaterial:
    def __init__(self, name):
        self.material = bpy.data.materials.new(name)
        self.material.use_nodes = True
        self.image_node = self.material.node_tree.nodes.new("ShaderNodeTexImage")
        self.image_node.select = True
        self.material.node_tree.nodes.active = self.image_node

    def set_image(self, image):
        self.image_node.image = image.image

