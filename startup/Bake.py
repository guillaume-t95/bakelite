import bpy
import Material
import Mesh
import Engine

def create_simple(high, low, cage, image):
    return Bake(high, low, cage, image)

def create_multigroups(high, low, cage, image):
    return MultiGroups(high, low, cage, image)

def create_color_id(mesh_object, image):
    return ColorIDBake(mesh_object, image)

class ColorIDBake:
    def __init__(self, mesh_object, image):
        self.mesh_object = mesh_object
        self.map = image

    def render(self):
        Engine.use_cycles()

        for i in range(0, self.mesh_object.material_count()):
            # Create materials
            mat = Material.create_shadeless()
            mat.set_image(self.map)
            self.mesh_object.assign_material_slot(i, mat)

        Mesh.deselect_all()
        Mesh.hide_all()

        self.mesh_object.show()
        self.mesh_object.select()
        self.mesh_object.set_active()

        bpy.ops.object.bake('INVOKE_DEFAULT',
            type='COMBINED',
            margin=16,
            use_selected_to_active=False,
            cage_extrusion=0.0,
            save_mode='INTERNAL',
            use_clear=True,
            use_cage=True,
            use_split_materials=False,
            use_automatic_name=False,
            uv_layer=""
        )

        # Loop through each material in the stack and replace entry with a shadeless flat material
        # for
        # bake_mat = Material.create_shadeless(Colors.color_id[0])


class Bake:
    def __init__(self, high, low, cage, image):
        self.high = high
        self.low = low
        self.cage = cage
        self.map = image


    def render(self):
        Engine.use_cycles()
        bake_mat = Material.create_normal("bake_material")
        bake_mat.set_image(self.map)

        Mesh.deselect_all()
        Mesh.hide_all()

        self.high.show()
        self.low.show()

        self.low.assign_material(bake_mat)
        self.low.select()
        self.high.select()
        self.low.set_active()

        bpy.ops.object.bake('INVOKE_DEFAULT',
            type='NORMAL',
            margin=16,
            use_selected_to_active=True,
            cage_extrusion=0.0,
            cage_object=self.cage.name,
            normal_space='TANGENT',
            normal_r='POS_X',
            normal_g='POS_Y',
            normal_b='POS_Z',
            save_mode='INTERNAL',
            use_clear=True,
            use_cage=True,
            use_split_materials=False,
            use_automatic_name=False,
            uv_layer=""
        )

class MultiGroups:
    def __init__(self, high, low, cage, image):
        self.high = high
        self.low = low
        self.cage = cage
        self.map = image

    def render(self):
        Engine.use_cycles()
        bake_mat = Material.create_normal("bake_material")
        bake_mat.set_image(self.map)
        clear = True

        groups = self.low.vertex_groups()

        for group in groups:
            if (group.name[0]) is "/":
                low = self.low.vertex_group(group.name).to_object(group.name + "_low")
                high = self.high.vertex_group(group.name).to_object(group.name + "_high")
                cage = self.cage.vertex_group(group.name).to_object(group.name + "_cage")

                Mesh.deselect_all()
                Mesh.hide_all()

                high.show()
                low.show()

                low.assign_material(bake_mat)
                low.select()
                high.select()
                low.set_active()

                bpy.ops.object.bake('INVOKE_DEFAULT',
                    type='NORMAL',
                    margin=16,
                    use_selected_to_active=True,
                    cage_extrusion=0.0,
                    cage_object=cage.name,
                    normal_space='TANGENT',
                    normal_r='POS_X',
                    normal_g='POS_Y',
                    normal_b='POS_Z',
                    save_mode='INTERNAL',
                    use_clear=clear,
                    use_cage=True,
                    use_split_materials=False,
                    use_automatic_name=False,
                    uv_layer=""
                )

                clear = False
