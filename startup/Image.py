import bpy

def create(name, width, height):
    return Image(name, width, height)

class Image:
    def __init__(self, name, width, height):
        self.image = bpy.data.images.new(name, width, height)
        self.image.file_format = 'PNG'

    def set_path(self, filepath):
        self.image.filepath_raw = filepath
        self.image.filepath = filepath

    def save_as(self, filepath):
        self.set_path(filepath)
        self.save()

    def save(self):
        self.image.save_render(self.image.filepath, bpy.context.scene)
