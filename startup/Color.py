def create(r, g, b, a):
    return Color(r, g, b, a)

class Color:
    def __init__(self, r, g, b, a):
        self.r = r
        self.g = g
        self.b = b
        self.a = a

    def assign_value(self, val):
        val[0] = self.r
        val[1] = self.g
        val[2] = self.b
        val[3] = self.a