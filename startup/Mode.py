import bpy

EDIT = 'EDIT'
OBJECT = 'OBJECT'

# current = bpy.context.mode

def to_edit():
    bpy.ops.object.mode_set(mode='EDIT', toggle=False)
    print("===Switched to edit mode")

def to_object():
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    print("===Switched to object mode")