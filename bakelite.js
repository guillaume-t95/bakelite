var child = require('child_process'),
    path = require('path'),
    chalk = require('chalk'),
    reg = /@@(.+)@@/,
    makePythonArgs,
    formatMs;

var blend = process.env.BLENDER_PATH;
if (!blend) {
  console.log(chalk.red('No BLEND_PATH environment variable specified'))
  process.exit()
}

formatMs = function(time) {
  if (time / 1000 > 1) {
    return time / 1000 + 's';
  } else {
    return time + 'ms';
  }
};

makePythonArgs = function(args) {
  var command = '',
      index, value;
  for (index in args) {
    value = args[index];
    command += index + '=' + value + ' ';
  }
  return command.trim();
};

module.exports = {
  normal: function(options) {
    var blenderInstance, command;

    var time = Date.now();

    console.log(chalk.cyan(chalk.magenta('(Normal) ') + options.sourcefile));

    if (!options.useGroups) {
      options.useGroups = 0;
      console.log("Using single object");
    } else {
      options.useGroups = 1;
      console.log("Using vertex groups");
    }

    command = (options.sourcefile + ' --factory-startup -b -P main.py -- ' + makePythonArgs(options)).split(' ');
    blenderInstance = child.spawn(blend, command, {env: {'BLENDER_USER_SCRIPTS': path.resolve()}});

    blenderInstance.stdout.on('data', function(data) {
      data = reg.exec(data);
      if (data) {
        console.log(chalk.green(data[1]));
      }
    });

    blenderInstance.stderr.on('data', function(data) {
      console.log(chalk.red(data));
    });

    blenderInstance.on('exit', function(error) {
      var deltaTime = Date.now() - time;
      if (!error) {
        console.log(chalk.green('Completed after ' + chalk.blue(formatMs(deltaTime))));
      }
    });
  },

  colorID: function(options) {
    var blenderInstance, command;

    var time = Date.now();

    console.log(chalk.cyan(chalk.yellow('(ColorID) ') + options.sourcefile));

    if (!options.useGroups) {
      options.useGroups = 0;
      // console.log("Using single object");
    } else {
      options.useGroups = 1;
      // console.log("Using vertex groups");
    }

    command = (options.sourcefile + ' --factory-startup -b -P color_id.py -- ' + makePythonArgs(options)).split(' ');
    blenderInstance = child.spawn(blend, command, {env: {'BLENDER_USER_SCRIPTS': path.resolve()}});

    blenderInstance.stdout.on('data', function(data) {
      data = reg.exec(data);
      if (data) {
        console.log(chalk.green(data[1]));
      }
    });

    blenderInstance.stderr.on('data', function(data) {
      console.log(chalk.red(data));
    });

    blenderInstance.on('exit', function(error) {
      var deltaTime = Date.now() - time;
      if (!error) {
        console.log(chalk.green('Completed after ' + chalk.blue(formatMs(deltaTime))));
      }
    });
  }
};
